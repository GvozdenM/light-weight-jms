package Impl;

import org.jetbrains.annotations.NotNull;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;


public class JMSlimQueueDestination extends JMSlimDestination implements Queue {
    private ConcurrentLinkedQueue<Message> messageQueue;
    private Notifier notifierThread = null;
    private int consumerToUse = 0;


    public JMSlimQueueDestination(String name) {
        super(name, TYPE_QUEUE);
        this.messageQueue = new ConcurrentLinkedQueue<>();
        this.notifierThread = new Notifier();
        this.notifierThread.start();
    }

    @Override
    public String getQueueName() throws JMSException {
        return this.getName();
    }

    public void put(@NotNull Message message) {
        Message msgConfigured = this.configMessage(message);

        if(null == msgConfigured) {
            logger.log("Cannot send null message. Aborting.");
            return;
        }
        //enqueue message for sending
        this.messageQueue.add(message);

        //awaken notifier thread so it reads
        //the message out of the queue
        this.notifierThread.deliver();
    }

    private synchronized boolean notifyNextConsumer(Message message) {
        if(!this.noConsumers()) {
            ArrayList<ConsumerNode> consumerNodes = consumers;

            if(consumerToUse >= consumerNodes.size()) {
                consumerToUse = 0;
            }

            ConsumerNode consumerNode = consumerNodes.get(consumerToUse);
            consumerToUse++;

            JMSlimMessageConsumer messageConsumer = (JMSlimMessageConsumer) consumerNode.getConsumer();

            try {
                messageConsumer.onMessage(message);
                return true;
            }
            catch (Exception e) {
                logger.log("Failed to deliver message to consumer on " + this.getName());
            }
        }
        return false;
    }

    private synchronized boolean noConsumers() {
        return 0 == consumers.size();
    }

    public synchronized void close() {
        try {
            wait(100);
            this.notifierThread.interrupt();

        } catch (InterruptedException interruptedException) {
            interruptedException.printStackTrace();
        }
    }

    class Notifier extends Thread {

        @Override
        public void run() {
            super.run();

            try {
                logger.log("Starting notifier thread for " + getQueueName());
                while(true) {
                    boolean fQueueMessages = true;

                    while(fQueueMessages) {
                        //deliver first message from queue if available
                        //do not check with size() since this is not
                        //a constant time operation on ConcurrentLinkedQueue
                        //simply get it

                        Message message = messageQueue.peek();

                        if(null != message) {
                            //we have a message so try to deliver it
                            if(notifyNextConsumer(message)) {
                                //message has been delivered so remove it from queue
                                messageQueue.remove(message);
                            }

                            //remove any expired messages left in the queue
                            this.removeExpiredMessages();

                            //wait and relinquish control to allow
                            //other threads to perform work before
                            //continuing to deliver next message in queue
                            sleep(10);
                        }
                        else {
                            //we don't have a message so we can
                            //break out of the loop and wait to be
                            //asked to deliver another message
                            fQueueMessages = false;
                        }
                    }

                    //wait to be notified of new message
                    this.waitForNotification();
                }
            }
            catch (InterruptedException interruptedException) {
                Thread.currentThread().interrupt();
                logger.log("Notifier thread " + this.getName() + " stopping!");
            }
            catch (Exception e) {
                logger.error("Caught exception while managing destination notifications");
                e.printStackTrace();
            }
        }

        private void removeExpiredMessages() {

            //cycle through all messages in queue
            //iterator on concurrent queue is not guaranteed to see
            //all the elements but it will at least see those that
            //were presented when it is called
            for (Message message : messageQueue) {
                //can the message expire?
                try {
                    if (0 != message.getJMSExpiration()) {

                        //message can expire so check if it has expired
                        if (message.getJMSTimestamp() + message.getJMSExpiration() >
                                (System.currentTimeMillis() / 1000)) {

                            //remove expired message
                            messageQueue.remove(message);
                        }
                    }
                }
                //failed to get some data from the message
                catch (Exception e) {
                    logger.log("Failed to remove expired message");
                    logger.log(e.getMessage());
                }
            }
        }

        private synchronized void waitForNotification() throws InterruptedException {
            logger.log("Notifier thread waiting");
            wait();
        }

        public synchronized void deliver() {
            try {
                super.notify();
            }
            catch (Exception e) {
                logger.error("Could not awaken notifier thread!");
                logger.error(e.getMessage());
            }
        }
    }
}
