package Impl;

import javax.jms.JMSException;
import javax.jms.Message;

public class JMSlimInternalMessageRouter extends MessageRouter {
    @Override
    public void routeMessageToDestination(Message message) {
        try {
            System.out.println("Router got " + message.getJMSMessageID());
            JMSlim jmslim = new JMSlim();

            JMSlimDestination messageDestination  = (JMSlimDestination) message.getJMSDestination();
            JMSlimQueueDestination destination = (JMSlimQueueDestination) jmslim.lookup(messageDestination.getId());
            destination.put(message);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}

