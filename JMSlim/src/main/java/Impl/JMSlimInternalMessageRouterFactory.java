package Impl;

public class JMSlimInternalMessageRouterFactory extends MessageRouterFactory {
    @Override
    public MessageRouter create() {
        return new JMSlimInternalMessageRouter();
    }
}
