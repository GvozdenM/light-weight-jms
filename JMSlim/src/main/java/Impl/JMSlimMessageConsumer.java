package Impl;

import Logging.JMSlimLogger;

import javax.jms.*;
import java.lang.IllegalStateException;

public class JMSlimMessageConsumer implements MessageConsumer {
    protected JMSlimLogger logger = JMSlimLogger.createLogger();
    protected String messageSelector;
    protected Destination destination;
    protected MessageListener messageListener = null;
    protected Message message = null;
    protected boolean clientThreadBlocking = false;
    protected boolean open = true;

    public JMSlimMessageConsumer() {
    }

    public JMSlimMessageConsumer(Destination destination, String messageSelector) {
        this.messageSelector = messageSelector;
        this.destination = destination;
    }

    @Override
    public String getMessageSelector() throws JMSException {
        return this.messageSelector;
    }

    @Override
    public MessageListener getMessageListener() throws JMSException {
        return this.messageListener;
    }

    @Override
    public void setMessageListener(MessageListener messageListener) throws JMSException {
        this.messageListener = messageListener;
    }

    @Override
    public Message receive() throws JMSException {
        return this.receive(0);
    }

    public synchronized void onMessage(Message message) throws IllegalStateException {
        //do we have an async message listener?
        if(null != messageListener) {
            //we do so call it
            this.logger.log("Async message listener triggered");
            this.messageListener.onMessage(message);
        }
        else {
            //do we have a synchronous receiver waiting?
            if(this.clientThreadBlocking) {
                this.logger.log("Synchronous message receiver found. Delivering...");

                //store message so client thread can retrieve it
                this.message = message;

                //try to awaken client thread
                try {
                    this.logger.log("Waking up client thread for message delivery");
                    //awaken client thread waiting on lock for this object
                    this.notifyAll();

                    //current thread is no longer needed so yield processor use
                    Thread.yield();
                }
                catch (Exception e) {
                    logger.log("Could not deliver message");
                    logger.log(e.getMessage());
                }
            }
        }
    }

    @Override
    public synchronized Message receive(long timeout) throws JMSException {
        Message msg = null;
        boolean elapsed = false;

        try {
            this.logger.log("Client started message reception");

            //set client block to true (method is synchronized) to allow JMS to know whether
            //there's a client to deliver to
            this.clientThreadBlocking = true;

            if(0 == timeout) {
                //while there's no message sleep the thread
                //thread is awoken by notifyAll
                while(null == this.message && this.open) {
                    wait();
                }
            }
            else {
                //attempt to get message if none is available
                //sleep for the timeout specified at try again
                if(null == this.message && this.open) {
                    this.logger.log("Waiting " + timeout + " seconds for message");
                    wait(timeout);
                }

                this.logger.log("Timeout elapsed or message was available returning message if one was received");
                this.clientThreadBlocking = false;
                return this.receiveNoWait();
            }

            //we got a message so we're not blocking anymore
            this.clientThreadBlocking = false;

            //store message locally and remove it from this object
            //to allow other threads to write a new message to this consumer
            msg = this.message;
            this.message = null;

            logger.log("Consumer received message" + msg.getJMSMessageID());
        }
        catch (Exception e) {
            logger.error("Attempted to wait for message but exception was thrown: " + e.getMessage());
        }

        return msg;
    }

    @Override
    public synchronized Message receiveNoWait() throws JMSException {
        Message message = this.message;
        this.message = null;
        return message;
    }

    @Override
    public synchronized void close() throws JMSException {
        this.logger.log("Starting to close message consumer");

        try {
            if(this.clientThreadBlocking) {
                this.logger.log("Client thread is blocking message consumer");
                wait(1000);
                ((JMSlimDestination) this.destination).removeConsumer(this);
                this.open = false;
                this.destination = null;
                this.messageListener = null;
            }
        }
        catch (Exception e) {
            this.logger.log("Failed to close consumer! " + e.getMessage());
        }
    }
}
