package Impl;

public abstract class MessageRouterFactory {
    public abstract MessageRouter create();
}
