package Impl;

import Networking.concurrent.LowThroughPutConcurrentSocketManagerFactory;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;

/**
 * Implementation of {@link javax.jms.ConnectionFactory} for JMSlim.
 * Methods create un-connected instance of JMSlimConnection.
 */
public class JMSlimConnectionFactory implements ConnectionFactory {

    /**
     * Create new connection with low throughput socket manager
     * @return Connection implemented by {@link JMSlimConnection}
     */
    @Override
    public Connection createConnection() {
        return new JMSlimConnection(
                new LowThroughPutConcurrentSocketManagerFactory(),
                new JMSlimInternalMessageRouterFactory(),
                new JMSlimConfig()
        );
    }

    @Override
    public Connection createConnection(String s, String s1) {
        return this.createConnection();
    }
}
