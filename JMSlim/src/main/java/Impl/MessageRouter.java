package Impl;

import javax.jms.Message;

public abstract class MessageRouter {
    public abstract void routeMessageToDestination(Message message);
}
