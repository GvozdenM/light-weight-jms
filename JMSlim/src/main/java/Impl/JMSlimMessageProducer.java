package Impl;

import Logging.JMSlimLogger;

import javax.jms.*;

public class JMSlimMessageProducer implements MessageProducer {
    protected JMSlimLogger logger = JMSlimLogger.createLogger();
    protected int deliveryMode = DeliveryMode.NON_PERSISTENT;
    protected int priority = Message.DEFAULT_PRIORITY;
    protected long timeToLive = Message.DEFAULT_TIME_TO_LIVE;
    protected boolean disableMessageID = false;
    protected boolean disableMessageTimestamp = false;
    protected Destination destination = null;

    public JMSlimMessageProducer(Destination destination) {
        this.destination = destination;
    }

    @Override
    public void send(Message message) throws JMSException {
        this.send(this.destination, message, this.deliveryMode, this.priority, this.timeToLive);
    }

    @Override
    public void send(Message message, int deliveryMode, int priority, long timeToLive) throws JMSException {
        this.send(destination, message, deliveryMode, priority, timeToLive);
    }

    @Override
    public void send(Destination destination, Message message) throws JMSException {
        this.send(destination, message, this.deliveryMode, this.priority, this.timeToLive);
    }

    @Override
    public void send(Destination destination, Message message, int deliveryMode, int priority, long timeToLive) throws JMSException {
        try {
            if(null != destination) {
                Message configuredMessage = configureMessage(
                        message,
                        deliveryMode,
                        priority,
                        timeToLive
                );

                if(destination instanceof Queue) {
                    JMSlimQueueDestination queue = (JMSlimQueueDestination) destination;
                    queue.putToRemote(configuredMessage, this);
                }
                else {
                    logger.error("Topics are not implemented!");
                    throw new JMSException("Not queue fuck you");
                }
            }
            else {
                logger.error("Can not handle null destination. Throwing exception");
                throw new JMSException("Destination can not be null");
            }
        }
        catch (Exception e) {
            logger.error("Couldn't send message");
            logger.error(e.getMessage());
        }
    }

    private Message configureMessage(Message message, int deliveryMode, int priority, long timeToLive) throws JMSException {
        //set header fields
        message.setJMSExpiration(timeToLive);
        message.setJMSPriority(priority);
        message.setJMSDeliveryMode(deliveryMode);

        if(!this.disableMessageTimestamp) {
            message.setJMSTimestamp(System.currentTimeMillis() / 1000);
        }

        if(!this.disableMessageID) {
            message.setJMSMessageID(JMSlim.generateMessageId((JMSlimDestination) destination));
        }

        return message;
    }

    public Destination getDestination()  throws JMSException
    {
        //Gets the destination associated with this Messag
        return destination;
    }

    public int getDeliveryMode()  throws JMSException
    {
        // Gets the producer's default delivery mode.
        return deliveryMode;
    }

    public boolean getDisableMessageID()  throws JMSException
    {
        // Gets an indication of whether message IDs are disabled.
        return disableMessageID;
    }

    public boolean getDisableMessageTimestamp()  throws JMSException
    {
        // Gets an indication of whether message timestamps are disabled.
        return disableMessageTimestamp;
    }

    public int getPriority()  throws JMSException
    {
        // Gets the producer's default priority.
        return priority;
    }

    public long getTimeToLive()  throws JMSException
    {
        // Gets the default length of time in milliseconds from its dispatch
        // time that a produced message should be retained by the message system
        return timeToLive;
    }

    public void setDeliveryMode(int deliveryMode)  throws JMSException
    {
        // Sets the producer's default delivery mode.
        this.deliveryMode = deliveryMode;
    }

    public void setDisableMessageID(boolean value)  throws JMSException
    {
        // Sets whether message IDs are disabled.
        this.disableMessageID = value;
    }

    public void setDisableMessageTimestamp(boolean value)  throws JMSException
    {
        // Sets whether message timestamps are disabled.
        this.disableMessageTimestamp = value;
    }

    public void setPriority(int priority)  throws JMSException
    {
        // Sets the producer's default priority.
        this.priority = priority;
    }

    public void setTimeToLive(long timeToLive)  throws JMSException
    {
        // Sets the default length of time in milliseconds from its dispatch
        // time that a produced message should be retained by the message system.
        this.timeToLive = timeToLive;
    }

    public void close()  throws JMSException
    {
        // Closes the message producer.
        logger.log("JMSLightMessageProducer - closing MessageProducer");
        ((JMSlimDestination)destination).removeProducer(this);
        destination = null;
    }


}
