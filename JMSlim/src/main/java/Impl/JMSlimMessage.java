package Impl;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;


public abstract class JMSlimMessage<T> implements Message, Serializable {
    protected String id;
    protected String transactionId;
    protected long timestamp;
    protected Destination replyTo;
    protected Destination destination;
    protected int deliveryMode = DeliveryMode.NON_PERSISTENT;
    protected boolean redelivered;
    protected String type;
    protected long expiration;
    protected T body;
    protected int JMSPriority;
    protected String destinationName;
    protected HashMap<String, String> stringProperties = new HashMap<>();

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    @Override
    public int getJMSPriority() {
        return JMSPriority;
    }

    @Override
    public void setJMSPriority(int JMSPriority) {
        this.JMSPriority = JMSPriority;
    }

    @Override
    public String getJMSMessageID() throws JMSException {
        return this.id;
    }

    @Override
    public void setJMSMessageID(String s) throws JMSException {
        this.id = s;
    }

    @Override
    public long getJMSTimestamp() throws JMSException {
        return timestamp;
    }

    @Override
    public void setJMSTimestamp(long l) throws JMSException {
        this.timestamp = l;
    }

    @Override
    public byte[] getJMSCorrelationIDAsBytes() throws JMSException {
        if(null != this.transactionId) {
            return this.transactionId.getBytes();
        }
        else {
            return new byte[0];
        }
    }

    @Override
    public void setJMSCorrelationIDAsBytes(byte[] bytes) throws JMSException {
        this.transactionId = Arrays.toString(bytes);
    }

    @Override
    public void setJMSCorrelationID(String s) throws JMSException {
        this.transactionId = s;
    }

    @Override
    public String getJMSCorrelationID() throws JMSException {
        return this.transactionId;
    }

    @Override
    public Destination getJMSReplyTo() throws JMSException {
        return this.replyTo;
    }

    @Override
    public void setJMSReplyTo(Destination destination) throws JMSException {
        this.replyTo = destination;
    }

    @Override
    public Destination getJMSDestination() throws JMSException {
        return this.destination;
    }

    @Override
    public void setJMSDestination(Destination destination) throws JMSException {
        this.destination = destination;
    }

    @Override
    public int getJMSDeliveryMode() throws JMSException {
        return this.deliveryMode;
    }

    @Override
    public void setJMSDeliveryMode(int i) throws JMSException {
        this.deliveryMode = i;
    }

    @Override
    public boolean getJMSRedelivered() throws JMSException {
        return this.redelivered;
    }

    @Override
    public void setJMSRedelivered(boolean b) throws JMSException {
        this.redelivered = b;
    }

    @Override
    public String getJMSType() throws JMSException {
        return this.type;
    }

    @Override
    public void setJMSType(String s) throws JMSException {
        this.type = s;
    }

    @Override
    public long getJMSExpiration() throws JMSException {
        return this.expiration;
    }

    @Override
    public void setJMSExpiration(long expiration) throws JMSException {
        this.expiration = expiration;
    }

    @Override
    public void clearProperties() throws JMSException {
        this.id = null;
        this.transactionId = null;
        this.timestamp = 0;
        this.replyTo = null;
        this.destination = null;
        this.deliveryMode = DeliveryMode.NON_PERSISTENT;
        this.redelivered = false;
        this.type = null;
        this.expiration = 0;
    }

    @Override
    public void clearBody() throws JMSException {
        this.body = null;
    }
}
