package Impl;

import Logging.JMSlimLogger;

import javax.jms.*;
import java.io.Serializable;

public class JMSlimSession implements Session {
    protected JMSlimLogger logger = JMSlimLogger.createLogger();
    protected boolean active = true;
    protected boolean transacted = false;
    protected int ackMode;
    protected Connection connection;

    public JMSlimSession(JMSlimConnection jmSlimConnection, boolean transact, int ackMode) {
        this.connection = jmSlimConnection;
        this.ackMode = ackMode;
    }

    @Override
    public MessageConsumer createConsumer(Destination destination) throws JMSException {
        return createConsumer(destination, null, false);
    }

    @Override
    public MessageConsumer createConsumer(Destination destination, String selector) throws JMSException {
        return createConsumer(destination, selector, false);
    }

    @Override
    public MessageConsumer createConsumer(Destination destination, String selector, boolean noLocal) throws JMSException {
        this.logger.log("Session is creating message consumer");


        if(null == destination) {
            logger.error("Destination is null cannot create consumer");
            throw new JMSException("Destination is null cannot create consumer");
        }
        else {
            if(!active) {
                logger.error("Session is closed cannot create consumer");
                throw new JMSException("Session is closed cannot create consumer");
            }
            else {
                if(destination instanceof Queue) {
                    MessageConsumer consumer = new JMSlimMessageConsumer(destination, selector);
                    ((JMSlimDestination) destination).addConsumer(consumer, this.connection);
                    return consumer;
                }
            }
        }
        logger.error("Destination cannot be null");
        throw new JMSException("Destination is not queue");
    }

    @Override
    public BytesMessage createBytesMessage() throws JMSException {
        return null;
    }

    @Override
    public MapMessage createMapMessage() throws JMSException {
        return null;
    }

    @Override
    public Message createMessage() throws JMSException {
        return null;
    }

    @Override
    public ObjectMessage createObjectMessage() throws JMSException {
        return new JMSlimObjectMessage();
    }

    @Override
    public ObjectMessage createObjectMessage(Serializable serializable) throws JMSException {
        return null;
    }

    @Override
    public StreamMessage createStreamMessage() throws JMSException {
        return null;
    }

    @Override
    public TextMessage createTextMessage() throws JMSException {
        return null;
    }

    @Override
    public TextMessage createTextMessage(String s) throws JMSException {
        return null;
    }

    @Override
    public boolean getTransacted() throws JMSException {
        return false;
    }

    @Override
    public int getAcknowledgeMode() throws JMSException {
        return 0;
    }

    @Override
    public void commit() throws JMSException {

    }

    @Override
    public void rollback() throws JMSException {

    }

    @Override
    public void close() throws JMSException {

    }

    @Override
    public void recover() throws JMSException {

    }

    @Override
    public MessageListener getMessageListener() throws JMSException {
        return null;
    }

    @Override
    public void setMessageListener(MessageListener messageListener) throws JMSException {

    }

    @Override
    public void run() {

    }

    @Override
    public MessageProducer createProducer(Destination destination) throws JMSException {
        MessageProducer producer = new JMSlimMessageProducer(destination);
        ((JMSlimDestination) destination).addProducer(producer, this.connection);
        return producer;
    }


    @Override
    public Queue createQueue(String s) throws JMSException {
        return null;
    }

    @Override
    public Topic createTopic(String s) throws JMSException {
        return null;
    }

    @Override
    public TopicSubscriber createDurableSubscriber(Topic topic, String s) throws JMSException {
        return null;
    }

    @Override
    public TopicSubscriber createDurableSubscriber(Topic topic, String s, String s1, boolean b) throws JMSException {
        return null;
    }

    @Override
    public QueueBrowser createBrowser(Queue queue) throws JMSException {
        return null;
    }

    @Override
    public QueueBrowser createBrowser(Queue queue, String s) throws JMSException {
        return null;
    }

    @Override
    public TemporaryQueue createTemporaryQueue() throws JMSException {
        return null;
    }

    @Override
    public TemporaryTopic createTemporaryTopic() throws JMSException {
        return null;
    }

    @Override
    public void unsubscribe(String s) throws JMSException {

    }
}
