package Impl;

import Logging.JMSlimLogger;
import org.jetbrains.annotations.NotNull;

import javax.jms.*;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Implements {@link javax.jms.Destination} abstracts away the behavior
 * common to Queues and Topics.
 */
public abstract class JMSlimDestination implements Destination, Serializable {
    protected static final String TYPE_QUEUE = "queue";
    protected static final String TYPE_TOPIC = "topic";

    protected final JMSlimLogger logger = JMSlimLogger.createLogger();

    protected transient final ArrayList<ProducerNode> producers;
    protected transient final ArrayList<ConsumerNode> consumers;
    private transient boolean killed;

    private final String name;
    private final String type;

    /**
     * Default constructor creates destination of given name and type
     * @param name Destination's name
     * @param type Intended to be set by subclasses (implementations of {@link javax.jms.Queue}
     *      * and {@link javax.jms.Topic}
     */
    protected JMSlimDestination(@NotNull String name, @NotNull String type) {
        this.name = name;
        this.type = type;
        this.producers = new ArrayList<>();
        this.consumers = new ArrayList<>();
    }

    /**
     * Return the identifier for this destination
     * @return String the identifier
     */
    public String getId() {
        return this.getType() + "/" + this.getName();
    }

    /**
     * Inner class contains information linking  a message producer and the connection to the
     * messaging sever it uses to deliver its messages
     */
    protected static class ProducerNode {
        private final JMSlimConnection connection;
        private final MessageProducer producer;

        /**
         * Default constructor
         * @param connection Instance of the connection the producer uses
         * @param producer A message producer
         */
        public ProducerNode(@NotNull JMSlimConnection connection, @NotNull MessageProducer producer) {
            this.connection = connection;
            this.producer = producer;
        }

        public JMSlimConnection getConnection() {
            return connection;
        }

        public MessageProducer getProducer() {
            return producer;
        }

    }

    /**
     * Inner class contains information linking a message consumer and the connection to the
     * messaging sever it uses to recieve its messages
     */
    protected static class ConsumerNode {
        private final Connection connection;
        private final MessageConsumer consumer;

        /**
         * Default constructor
         * @param connection Instance of the connection the producer uses
         * @param consumer A message consumer
         */
        public ConsumerNode(@NotNull Connection connection, @NotNull MessageConsumer consumer) {
            this.connection = connection;
            this.consumer = consumer;
        }

        public Connection getConnection() {
            return connection;
        }

        public MessageConsumer getConsumer() {
            return consumer;
        }
    }

    /**
     * Configures a message for sending
     * @param message Non-null message
     * @return Message configured
     */
    protected Message configMessage(@NotNull Message message) {
        try {
            //add queue specific message configuration
            //noinspection rawtypes
            JMSlimMessage jmSlimMessage = (JMSlimMessage) message;
            jmSlimMessage.setDestinationName(this.getId());
            jmSlimMessage.setJMSDestination(this);
        }
        catch (Exception e) {
            logger.error(e.getMessage());
        }

        return message;
    }

    /**
     * Abstract interface for putting a message to this destination.
     * Intended for internal message router to post to local destinations
     * @param message The message to be posted to this destination
     */
    abstract void put(@NotNull Message message);

    /**
     * Method available to allow MessageProducers to send this message
     * to a remote destination by this name.
     * @param message The message to send
     * @param producer The message producer sending
     */
    void putToRemote(@NotNull Message message, @NotNull MessageProducer producer) {
        Message configuredMessage = configMessage(message);

        for(ProducerNode pn : this.producers) {
            if(pn.getProducer().equals(producer)) {
                JMSlimConnection connection = pn.getConnection();
                connection.deliver(configuredMessage);
                break;
            }
        }
    }

    /**
     * Registers a message consumer with this destination
     * @param consumer The message consumer
     * @param connection The connection to consume from
     * @throws JMSException Thrown if destination has been stopped
     */
    public void addConsumer(MessageConsumer consumer, Connection connection) throws JMSException {
        if(!killed) {
            this.consumers.add(new ConsumerNode(connection, consumer));
        }
        else {
            throw new JMSException("Can not use dead destination!");
        }
    }

    /**
     * Unregister a consumer from this destination
     * @param consumer The consumer to unregister
     * @throws JMSException Thrown if this destination has been stopped
     */
    public void removeConsumer(MessageConsumer consumer) throws JMSException {
        if(!killed) {
            this.consumers.removeIf(consumerNode -> consumerNode.consumer.equals(consumer));
        }
        else {
            throw new JMSException("Can not use dead destination!");
        }
    }

    /**
     * Register a message producer with this destination
     * @param producer The message producer to register
     * @param connection The connection to use for message delivery
     * @throws JMSException Thrown if this destination has been stopped
     */
    public void addProducer(MessageProducer producer, Connection connection) throws JMSException {
        if(!killed) {
            this.producers.add(new ProducerNode((JMSlimConnection) connection, producer));
        }
        else {
            throw new JMSException("Can not use dead destination!");
        }
    }

    /**
     * Unregister a message producer from this destination
     * @param producer The producer to unregister
     * @throws JMSException Thrown if this destination has been stopped
     */
    public void removeProducer(MessageProducer producer) throws JMSException {
        if(!killed) {
            this.producers.removeIf(producerNode -> producerNode.producer.equals(producer));
        }
        else {
            throw new JMSException("Can not use dead destination!");
        }
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public boolean isKilled() {
        return killed;
    }

    public void setKilled(boolean killed) {
        this.killed = killed;
    }
}
