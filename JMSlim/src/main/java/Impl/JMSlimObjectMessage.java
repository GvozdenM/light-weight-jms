package Impl;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import java.io.Serializable;
import java.util.Enumeration;

public class JMSlimObjectMessage extends JMSlimMessage<Serializable> implements ObjectMessage {
    @Override
    public void setObject(Serializable serializable) throws JMSException {
        this.body = serializable;
    }

    @Override
    public Serializable getObject() throws JMSException {
        return this.body;
    }

    @Override
    public boolean propertyExists(String s) throws JMSException {
        throw new RuntimeException("Unimplemented");

    }

    @Override
    public boolean getBooleanProperty(String s) throws JMSException {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public byte getByteProperty(String s) throws JMSException {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public short getShortProperty(String s) throws JMSException {
        throw new RuntimeException("Unimplemente1d");
    }

    @Override
    public int getIntProperty(String s) throws JMSException {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public long getLongProperty(String s) throws JMSException {
        return 0;
    }

    @Override
    public float getFloatProperty(String s) throws JMSException {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public double getDoubleProperty(String s) throws JMSException {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public String getStringProperty(String s) throws JMSException {
        return this.stringProperties.getOrDefault(s, "");
    }

    @Override
    public Object getObjectProperty(String s) throws JMSException {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public Enumeration getPropertyNames() throws JMSException {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public void setBooleanProperty(String s, boolean b) throws JMSException {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public void setByteProperty(String s, byte b) throws JMSException {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public void setShortProperty(String s, short i) throws JMSException {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public void setIntProperty(String s, int i) throws JMSException {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public void setLongProperty(String s, long l) throws JMSException {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public void setFloatProperty(String s, float v) throws JMSException {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public void setDoubleProperty(String s, double v) throws JMSException {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public void setStringProperty(String s, String s1) throws JMSException {
        this.stringProperties.put(s, s1);
    }

    @Override
    public void setObjectProperty(String s, Object o) throws JMSException {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public void acknowledge() throws JMSException {
        throw new RuntimeException("Unimplemented");
    }
}
