import Impl.JMSlim;
import Impl.JMSlimObjectMessage;
import Logging.JMSlimLogger;
import Networking.SocketManager;
import Networking.concurrent.ConcurrentSocketManager;

import javax.jms.*;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) {
        try {
            ConcurrentSocketManager socketManager = new ConcurrentSocketManager(new Socket("127.0.0.1", 3337));
            socketManager.start();
            socketManager.send("kek'sId");

            ConcurrentSocketManager socketManager2 = new ConcurrentSocketManager(new Socket("127.0.0.1", 3337));
            socketManager2.setOnReceived((o) -> {
                try {
                    System.out.println("ECHO " + ((ObjectMessage) o).getObject());
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            });
            socketManager2.start();
            socketManager2.send("kek2'sId");

            JMSlimObjectMessage objectMessage = new JMSlimObjectMessage();
            objectMessage.setObject("Hello!");

            objectMessage.setStringProperty("clientId", "kek2'sId");

            socketManager.send(objectMessage);

            JMSlimObjectMessage objectMessage2 = new JMSlimObjectMessage();
            objectMessage2.setObject("I'm 'a pull you to this bullet and put it through you");
            objectMessage2.setStringProperty("clientId", "kek2'sId");

            socketManager.send(objectMessage2);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    public static void testConcurrentSocketManager() {
        JMSlimLogger.createLogger().toggleDebugMode();
        try {

            ExecutorService server = Executors.newFixedThreadPool(10);
            server.execute(() -> {
                final ServerSocket serverSocket;
                final SocketManager socketManager;
                try {
                    serverSocket = new ServerSocket(3300);
                    socketManager = new ConcurrentSocketManager(serverSocket.accept());

                    socketManager.setOnReceived(System.out::println);
                    socketManager.start();

                    socketManager.send("from server Kek 1");
                    socketManager.send("from server Kek 2");
                    socketManager.send("from server Kek 3");
                    socketManager.send("from server Kek 4");
                    socketManager.send("from server Kek 5");
                    socketManager.send("from server Kek 6");



                    Thread.sleep(100);
//                    socketManager.close();
                    socketManager.close();
                }
                catch (InterruptedException e) {
                    System.out.println("Interuptted while sleeping byeee!!!");
                    Thread.currentThread().interrupt();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            });

            Thread.sleep(15);
            SocketManager socketManager = new ConcurrentSocketManager(new Socket("127.0.0.1", 3300));
            socketManager.setOnReceived(System.out::println);

            socketManager.start();
            server.execute(() ->{

                socketManager.send("Kek 1");
                socketManager.send("Kek 2");
                socketManager.send("Kek 3");

                socketManager.send("Kek 4");
                socketManager.send("Kek 5");
                socketManager.send("Kek 6");


            });
//            Socket socket = new Socket("127.0.0.1", 3300);
//            ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
//            outputStream.writeObject("Fuck off");
//            outputStream.writeObject("Fuck off again...");
//            outputStream.writeObject("Fuck off a third time...");
//            outputStream.writeObject("Fuck off a fourth time...");

            Thread.sleep(100);
            server.shutdown();

            if(!server.awaitTermination(150, TimeUnit.MILLISECONDS)) {
                server.shutdownNow();
                socketManager.close();
            }

            System.out.println("piece out mofos");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void testMessages() {
        JMSlim jmSlim = JMSlim.getJMSlim();
        try {
            Destination destination = jmSlim.createQueue("ECHO");

            ConnectionFactory connectionFactory = (ConnectionFactory) jmSlim.lookup("ConnectionFactory");
            Connection connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageConsumer consumer = session.createConsumer(destination);

            Thread producerThread = new Thread(() -> {
                try {
                    JMSlim jmSlim1 = JMSlim.getJMSlim();

                    Destination destination1 = jmSlim1.createQueue("ECHO");
                    ConnectionFactory connectionFactory1 = (ConnectionFactory) jmSlim1.lookup("ConnectionFactory");
                    Connection connection1 = connectionFactory1.createConnection();
                    Session session1 = connection1.createSession(false, Session.AUTO_ACKNOWLEDGE);
                    MessageProducer producer = session1.createProducer(destination1);

                    ObjectMessage objectMessage = session1.createObjectMessage();
                    objectMessage.setObject("M1 Hello");

                    ObjectMessage objectOutput = session1.createObjectMessage();
                    objectOutput.setObject("M2 Hello again");

                    producer.send(objectMessage);

                    Thread.sleep(5000);
                    producer.send(objectOutput);

                    System.out.println("Done producing!");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

            producerThread.start();
            System.out.println("ECHO: " + ((ObjectMessage) consumer.receive()).getObject());
            System.out.println("ECHO: " + ((ObjectMessage) consumer.receive()).getObject());

        }
        catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Done!");

        JMSlim.getJMSlim().close();
    }
}
