package Logging;

public class JMSlimLogger {
    private static final int FULL_LOG_MODE = 1;
    private static final int ERROR_ONLY_MODE = 2;
    private static final JMSlimLogger INSTANCE = new JMSlimLogger(FULL_LOG_MODE);

    private int mode;
    private int logLineCount;
    private int errorCount;

    private JMSlimLogger(int mode) {
        this.mode = mode;
    }

    public static JMSlimLogger createLogger() {
        return INSTANCE;
    }

    public void log(String s) {
        if(FULL_LOG_MODE == this.mode) {
            StringBuilder builder = new StringBuilder()
                    .append("JMSlim Log ")
                    .append("[")
                    .append(this.logLineCount)
                    .append("] ")
                    .append(s);
            System.out.println(builder.toString());
            this.logLineCount++;
        }
    }

    public void error(String s) {
        StringBuilder builder = new StringBuilder()
                .append("JMSlim Error ")
                .append("[")
                .append(this.errorCount)
                .append("] ")
                .append(s);
        System.out.println(builder.toString());
        this.errorCount++;
    }

    public void toggleDebugMode() {
        if(FULL_LOG_MODE == this.mode) {
            this.mode = ERROR_ONLY_MODE;
        }
        else {
            this.mode = FULL_LOG_MODE;
        }
    }
}
