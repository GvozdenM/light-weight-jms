package Networking;

public interface NetworkServiceFactory {
    NetworkService create();
}
