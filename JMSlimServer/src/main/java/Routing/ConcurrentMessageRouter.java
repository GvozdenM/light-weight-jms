package Routing;

import Handlers.ConnectionHandler;
import Impl.JMSlimMessage;
import Impl.JMSlimObjectMessage;
import Networking.NetworkService;
import Networking.SocketManager;
import ServiceImpl.NetworkExecutorService;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import java.net.Socket;

public class ConcurrentMessageRouter extends ConnectionHandler {
    public ConcurrentMessageRouter(Socket socketManager) {
        super(socketManager);
    }

    @Override
    public void run() {
        this.lock.lock();
        try {
            this.socketManager.start();
            this.lock.unlock();

            this.lock.lock();
            this.socketManager.setOnReceived((message) -> {
                if(message instanceof Message) {
                    try {
                       JMSlimObjectMessage objectMessage = (JMSlimObjectMessage) message;
                       String clientConnectionId = objectMessage.getStringProperty("clientId");
                       SocketManager clientSocketManager = NetworkExecutorService.getConnectionById(clientConnectionId);

                       if(null != clientSocketManager) {
                           clientSocketManager.send(message);
                       }
                    } catch (JMSException ignored) {}
                }
            });
        }
        catch (Exception e){}
        finally {
            this.lock.unlock();
        }
    }
}
