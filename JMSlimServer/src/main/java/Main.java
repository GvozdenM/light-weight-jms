import Routing.ConcurrentMessageRouterFactory;
import Networking.NetworkService;
import Networking.concurrent.ConcurrentSocketManagerFactory;
import ServiceImpl.NetworkExecutorService;

public class Main {
    public static void main(String[] args) {
        NetworkService networkService = new NetworkExecutorService(
                new ConcurrentMessageRouterFactory(),
                new ConcurrentSocketManagerFactory(),
                3337,
                2
        );

        networkService.run();
    }
}
