package Handlers;

import Networking.NetworkService;
import Networking.SocketManager;
import Networking.concurrent.ConcurrentSocketManager;
import ServiceImpl.NetworkExecutorService;

import java.net.Socket;
import java.util.concurrent.locks.ReentrantLock;

public abstract class ConnectionHandler implements Runnable {
    protected final SocketManager socketManager;
    protected final ReentrantLock lock = new ReentrantLock(true);

    public ConnectionHandler(Socket socket) {
        this.socketManager = new ConcurrentSocketManager(socket);
        this.socketManager.setInterceptNextListener((o) -> {
            if(o instanceof String) {
                this.lock.lock();
                NetworkExecutorService.addClient((String) o, this.socketManager);
                this.lock.unlock();
            }
        });
    }
}
