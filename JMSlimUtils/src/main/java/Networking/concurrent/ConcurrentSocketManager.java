package Networking.concurrent;

import Callbacks.interfaces.OnSuccess;
import org.jetbrains.annotations.NotNull;
import Networking.SocketManager;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Queue;
import java.util.concurrent.*;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

public class ConcurrentSocketManager extends SocketManager {
    private static final int DEFAULT_POOL_SIZE = 3;

    private final ExecutorService notifierThreadPool;
    private final ExecutorService outputQueueHandlerThread = Executors.newSingleThreadExecutor();
    private final ExecutorService inputQueueHandler = Executors.newSingleThreadExecutor();
    private final ReentrantLock mainLock = new ReentrantLock();
    private final Queue<Object> outgoingObjectQueue = new ConcurrentLinkedQueue<>();
    private ObjectInputStream objectInputStream;
    private ObjectOutputStream objectOutputStream;
    private final ReentrantLock inputStreamLock = new ReentrantLock(true);
    private final ReentrantLock outputStreamLock = new ReentrantLock(true);
    private Consumer<Object> interceptNextListener;

    /**
     * Constructor to create an non-setup SocketManager. To use
     * socket manager {@link SocketManager#start(String, int)} should be called.
     * The socket manager will connect the socket. To receive objects through the socket
     * input stream {@link SocketManager#onReceived} must be set using
     * {@link SocketManager#setOnReceived(OnSuccess)}
     */
    public ConcurrentSocketManager() {
        super();
        this.notifierThreadPool = Executors.newFixedThreadPool(DEFAULT_POOL_SIZE);
    }

    /**
     * Constructor to create an non-setup SocketManager. To use
     * socket manager {@link SocketManager#start(String, int)} should be called.
     * The socket manager will connect the socket. To receive objects through the socket
     * input stream {@link SocketManager#onReceived} must be set using
     * {@link SocketManager#setOnReceived(OnSuccess)}. Notifier thread pool size is
     * configurable
     *
     * @param poolSize Size of notifier thread pool
     */
    public ConcurrentSocketManager(int poolSize) {
        super();

        if(poolSize > 1) {
            this.notifierThreadPool = Executors.newFixedThreadPool(poolSize);
        }
        else {
            this.notifierThreadPool = Executors.newFixedThreadPool(DEFAULT_POOL_SIZE);
        }
    }

    /**
     * Constructor used to manage an already connected socket. The socket
     * should not be access in the mean time by any other object or thread.
     * @param socket Connected socket
     */
    public ConcurrentSocketManager(@NotNull Socket socket) {
        super(socket);
        this.notifierThreadPool = Executors.newFixedThreadPool(DEFAULT_POOL_SIZE);
    }

    /**
     * Constructor used to manage an already connected socket with sending and
     * receiving messages. Message reception thread count is configurable.
     * The socket should not be access in the mean time by
     * any other object or thread.
     * @param poolSize Size of notifier thread pool
     */
    public ConcurrentSocketManager(@NotNull Socket socket, int poolSize) {
        super(socket);

        if(poolSize > 1) {
            this.notifierThreadPool = Executors.newFixedThreadPool(poolSize);
        }
        else {
            this.notifierThreadPool = Executors.newFixedThreadPool(DEFAULT_POOL_SIZE);
        }
    }

    /**
     * Constructor used to manage an already connected socket with sending and
     * receiving messages. The socket should not be access in the mean time by
     * any other object or thread.
     * @param socket Connected socket
     * @param onReceived Callback to launch whenever a message is received
     *                   on the socket's input stream. The callback is triggered
     *                   each time a message is recieved on a seperate handler thread.
     */
    public ConcurrentSocketManager(@NotNull Socket socket, @NotNull OnSuccess<Object> onReceived) {
        super(socket, onReceived);
        this.notifierThreadPool = Executors.newFixedThreadPool(DEFAULT_POOL_SIZE);
    }

     /**
     * Constructor used to manage an already connected socket with sending and
     * receiving messages. Message reception thread count is configurable.
      * The socket should not be access in the mean time by
     * any other object or thread.
      * @param socket Connected socket
      * @param onReceived Callback to launch whenever a message is received
      *                   on the socket's input stream. The callback is triggered
      *                   each time a message is recieved on a seperate handler thread.
     * @param poolSize Size of notifier thread pool
     */
    public ConcurrentSocketManager(@NotNull Socket socket, @NotNull OnSuccess<Object> onReceived, int poolSize) {
        super(socket, onReceived);

        if(poolSize > 1) {
            this.notifierThreadPool = Executors.newFixedThreadPool(poolSize);
        }
        else {
            this.notifierThreadPool = Executors.newFixedThreadPool(DEFAULT_POOL_SIZE);
        }
    }


    /**
     * Class handles specifics to asynchronously receiving objects
     * through teh socket asynchronously. Implements Runnable to be submittable
     * to executor service for execution.
     */
    private class ReceptionHandler implements Runnable {
        @Override
        public void run() {
            //try to run with socket
            consumeSocketIfOpen((socket) -> {
                try {
                    //run reception looper
                    looper();
                } catch (IOException | ClassNotFoundException | RejectedExecutionException ignored) {}
                //shutting down
                catch (InterruptedException interruptedException) {
                    Thread.currentThread().interrupt();
                }
                //shutting down
                finally {
                    //check if we were interrupted while holding the lock
                    if (inputStreamLock.isHeldByCurrentThread()) {
                        inputStreamLock.unlock();
                    }
                }
            });
        }

        private void looper() throws InterruptedException, IOException, ClassNotFoundException {
            //break loop when thread is interrupted
            while (!Thread.currentThread().isInterrupted()) {

                //lock on to input stream or throw interrupted exception
                //if interrupted while waiting
                inputStreamLock.lockInterruptibly();

                //read one object from the stream
                final Object o = objectInputStream.readObject();

                //release input stream lock
                inputStreamLock.unlock();

                //we read the stop signal so break
                if (o.equals("-1")) break;

                //queue up callback to be execute on thread pool
                notifierThreadPool.execute(() -> {
                    mainLock.lock();
                       try {
                           if(null != interceptNextListener) {
                               interceptNextListener.accept(o);
                               interceptNextListener = null;
                           }
                           else {
                               onReceived.onSuccess(o);
                           }
                       }
                       finally {
                           mainLock.unlock();
                       }
                });
            }
        }
    }

    /**
     * Class handles business rules specific to sending messages
     * from the queue over the socket. Implements runnable such that
     * the synchronous queue clearing can be queued up as an asynchronous
     * task on ExecutorService
     */
    private class SendHandler implements Runnable {
        @Override
        public void run() {
            try {
                //try to acquire output stream lock and
                //stop blocking if thread is interrupted
                outputStreamLock.lockInterruptibly();

                //execute on socket if open
                consumeSocketIfOpen((socket) -> {
                    try {
                        //send each object in the queue
                        for (final Object o : outgoingObjectQueue) {
                            objectOutputStream.writeObject(o);
                            outgoingObjectQueue.remove(o);
                        }
                    } catch (IOException ignored) {}
                });
            }
            //we are stopping the service
            catch (InterruptedException e) {
                System.out.println("Stopping send thread!");
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            //always release the lock
            finally {
                //check if we were interrupted while holding the lock
                assert outputStreamLock.isHeldByCurrentThread();
                //release lock if we own it
                outputStreamLock.unlock();
            }
        }
    }

    /**
     * Send an object asynchronously via the socket
     *
     * @param object The object to be sent via the socket
     * @param <K> Captured type of object
     */
    @Override
    public <K> void send(@NotNull K object) {

        //acquire output stream lock
        this.outputStreamLock.lock();
        try {

            //add the object to the queue
            this.outgoingObjectQueue.add(object);

                //queue up clear queue operation on executor
                this.outputQueueHandlerThread.execute(new SendHandler());
        }
        //all exceptions ignored
        catch (Exception ignored) {}
        finally {
            //check if we were interrupted while holding the lock
            assert this.outputStreamLock.isHeldByCurrentThread();
            //release lock if we own it
            this.outputStreamLock.unlock();
        }
    }

    /**
     * Start receiving messages on socket input stream
     */
    private void startReceiving() {
        this.inputQueueHandler.execute(new ReceptionHandler());
    }

    /**
     * Create necessary streams and start recieving as they come in
     * and sending objects when requested.
     * @throws IOException Thrown if we can not open socket streams
     */
    @Override
    public void start() throws IOException {
        this.inputStreamLock.lock();
        this.outputStreamLock.lock();
        this.initStreams();
        this.startReceiving();
        this.active = true;
        this.inputStreamLock.unlock();
        this.outputStreamLock.unlock();
    }

    @Override
    public void setInterceptNextListener(Consumer<Object> callback) {
       this.mainLock.lock();
       this.interceptNextListener = callback;
       this.mainLock.unlock();
    }

    /**
     * Initiates socket streams
     * @throws IOException If streams couldn't open
     */
    private void initStreams() throws IOException {
        this.inputStreamLock.lock();
        this.outputStreamLock.lock();

        this.objectOutputStream = new ObjectOutputStream(this.socket.getOutputStream());
        this.objectInputStream = new ObjectInputStream(this.socket.getInputStream());

        this.inputStreamLock.unlock();
        this.outputStreamLock.unlock();
    }

    /**
     * Wait for all executors to stop
     * @return Have all executors stopped
     * @throws InterruptedException Thrown if thread has been interrupted while waiting
     */
    private boolean awaitExecutorShutdown() throws InterruptedException {
        return
                !this.inputQueueHandler.awaitTermination(150, TimeUnit.MILLISECONDS) ||
                        !this.outputQueueHandlerThread.awaitTermination(150, TimeUnit.MILLISECONDS) ||
                        !this.notifierThreadPool.awaitTermination(150, TimeUnit.MILLISECONDS);
    }

    /**
     * Try to soft shutdown executors and await termination
     * @return All executors terminated after soft stop
     * @throws InterruptedException Thread was interrupted while waiting on executors
     */
    private boolean softStopExecutors() throws InterruptedException {
        this.inputQueueHandler.shutdown();
        this.outputQueueHandlerThread.shutdown();
        this.notifierThreadPool.shutdown();
        return awaitExecutorShutdown();
    }

    /**
     * Hard shutdown all executors
     */
    private void hardStopExecutors() {
        this.inputQueueHandler.shutdownNow();
        this.outputQueueHandlerThread.shutdownNow();
        this.notifierThreadPool.shutdownNow();
    }

    /**
     * Stop executors slowly.
     */
    private void stopExecutors() {
        try {
            if(!this.softStopExecutors()) {
                 this.hardStopExecutors();
                 this.active = false;
            }
        }
        catch (InterruptedException ignored) {}
    }

    /**
     * Stops all object reception and sending and closes
     * the socket connection.
     * @throws IOException Various socket erros
     */
    @Override
    public void close() throws IOException {
        try {
            stopExecutors();
            this.outputStreamLock.lock();
            this.objectOutputStream.writeObject("-1");

            this.inputStreamLock.lock();
            this.socket.close();
        }
        catch (Exception ignored) {}
    }
}
