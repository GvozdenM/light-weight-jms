package Networking.concurrent;

import Networking.SocketManager;
import Networking.SocketManagerFactory;

import java.net.Socket;

public class ConcurrentSocketManagerFactory extends SocketManagerFactory {

    @Override
    public SocketManager create(Socket socket) {
        return new ConcurrentSocketManager(socket);
    }

    @Override
    public SocketManager createUnconnected() {
        return new ConcurrentSocketManager();
    }
}
