package Networking;

import java.net.Socket;

public abstract class SocketManagerFactory {
    public abstract SocketManager create(Socket socket);

    public abstract SocketManager createUnconnected();
}
