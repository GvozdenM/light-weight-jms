package Networking;

import Callbacks.interfaces.OnSuccess;
import org.jetbrains.annotations.NotNull;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.function.Consumer;

public abstract class SocketManager implements Closeable {
    protected Socket socket;
    protected boolean active;
    protected OnSuccess<Object> onReceived = (o)->{};

    /**
     * Constructor used to manage an already connected socket. The socket
     * should not be access in the mean time by any other object or thread.
     * @param socket Connected socket
     */
    public SocketManager(@NotNull Socket socket) {
        this.socket = socket;
        this.active = false;
    }

    /**
     * Constructor used to manage an already connected socket with sending and
     * receiving messages. The socket should not be access in the mean time by
     * any other object or thread.
     * @param socket Connected socket
     * @param onReceived Callback to launch whenever a message is received
     *                   on the socket's input stream.
     */
    public SocketManager(@NotNull Socket socket, @NotNull OnSuccess<Object> onReceived) {
        this.socket = socket;
        this.onReceived = onReceived;
        this.active = false;
    }

    /**
     * Constructor to create an non-setup SocketManager. To use start the
     * socket manager {@link SocketManager#start(String, int)} should be used.
     * The socket manager will connect the socket. To receive objects through the socket
     * input stream {@link SocketManager#onReceived} must be set using
     * {@link SocketManager#setOnReceived(OnSuccess)}
     */
    public SocketManager() {
        this.socket = new Socket();
        this.active = false;
    }

    /**
     * Send an object via the socket
     *
     * @param object The object to be sent via the socket
     * @param <K> Captured type of object
     */
    public abstract <K> void send(@NotNull K object);

    /**
     * Start receiving and sending messages via the already connected  socket
     * @throws IOException IOException if socket streams throw an exception
     * while being opened.
     */
    public abstract void start() throws IOException;

    /**
     * Connects the socket to a socket on the specified address
     * and port then start the SocketManager normally. This method
     * is equivalent to starting SocketManager using the {@link SocketManager#SocketManager(Socket)}
     * with an already constructed socket using {@link Socket#Socket(String, int)}
     * @param ip Address to connect to
     * @param port The port on the address to connect to
     * @throws IOException Throws IOException if starting the socket streams or
     * connecting the socket fails
     */
    public void start(@NotNull String ip, int port) throws IOException {

        //address already @NotNull
        //check port is not 0
        if(0 == port) {
            throw new IOException("Port cannot be 0");
        }

        //connect the socket
        this.socket.connect(new InetSocketAddress(ip, port));

        //start this service normally
        this.start();
    }

    /**
     * Protected utility method for running a method with the socket
     * if the socket is connected.
     * @param socketConsumer Consumer to accept the socket manager's
     *                       internally held socket instance
     */
    protected void consumeSocketIfOpen(Consumer<Socket> socketConsumer) {
        if(this.socket.isConnected() && !this.socket.isClosed()) {
            socketConsumer.accept(this.socket);
        }
    }

    /**
     * Setter method for incoming message callback
     * @param onReceived Callback called when an object is received
     */
    public void setOnReceived(OnSuccess<Object> onReceived) {
        this.onReceived = onReceived;
    }

    /**
     * Check if SocketManager is active.
     * @return is SocketManager active
     */
    public boolean isActive() {
        return active;
    }

    public abstract void setInterceptNextListener(Consumer<Object> callback);

    public String getLocalAddress() {
        return this.socket.getLocalAddress().getHostAddress();
    }

    public int getLocalPort() {
        return this.socket.getLocalPort();
    }
}
