package Callbacks.interfaces;

@FunctionalInterface
public interface OnSuccess<T> {
    void onSuccess(T d);
}
