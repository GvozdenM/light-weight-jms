package Callbacks.interfaces;

public interface OnException {
    void onException(Exception exception);
}
