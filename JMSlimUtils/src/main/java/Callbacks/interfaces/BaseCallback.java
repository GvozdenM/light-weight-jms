package Callbacks.interfaces;

public interface BaseCallback<S> extends OnSuccess<S>, OnException {
}
