package Callbacks;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.function.Consumer;

public class CallbackAdapter<S> {
    protected final Consumer<S> onSuccessCallback;
    protected final Consumer<Throwable> onFailCallback;
    protected Consumer<Throwable> onExceptionCallback;

    public CallbackAdapter(@NotNull Consumer<S> onSuccessCallback,
                           @Nullable Consumer<Throwable> onFailCallback,
                           @Nullable Consumer<Throwable> onExceptionCallback) {
        this.onSuccessCallback = onSuccessCallback;
        this.onFailCallback = null != onFailCallback ? onFailCallback : (t) -> {};
        this.onExceptionCallback = null != onExceptionCallback ? onExceptionCallback : (t) -> {};
    }

    public void onSuccess(S response) {
        this.onSuccessCallback.accept(response);
    }

    public void onFail(Throwable response) {
        this.onFailCallback.accept(response);
    }

    public void onException(Throwable response) {
        this.onExceptionCallback.accept(response);
    }
}
