package Callbacks.impl;


import Callbacks.interfaces.BaseCallback;
import Callbacks.interfaces.OnException;
import Callbacks.interfaces.OnSuccess;

public class ExceptionCallbackAdapter<S> extends BaseCallbackAdapter<S> implements BaseCallback<S> {
    private final OnException onExceptionCallback;


    public ExceptionCallbackAdapter(OnSuccess<S> onSuccessCallback,
                                    OnException onFailCallback) {
        super(onSuccessCallback);
        this.onExceptionCallback = null != onFailCallback ? onFailCallback : (t -> {});
    }

    @Override
    public void onException(Exception exception) {
        this.onExceptionCallback.onException(exception);
    }
}
