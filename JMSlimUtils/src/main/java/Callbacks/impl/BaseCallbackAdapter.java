package Callbacks.impl;

import Callbacks.interfaces.OnSuccess;

public abstract class BaseCallbackAdapter<S> implements OnSuccess<S> {
    private final OnSuccess<S> onSuccessCallback;

    protected BaseCallbackAdapter(OnSuccess<S> onSuccessCallback) {
        this.onSuccessCallback = null != onSuccessCallback ? onSuccessCallback: (t -> {});
    }

    @Override
    public final void onSuccess(S d) {
        this.onSuccessCallback.onSuccess(d);
    }
}
